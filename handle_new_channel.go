package main

import (
	"fmt"

	"golang.org/x/crypto/ssh"
)

func handleNewChannel(ch ssh.NewChannel) error {
	if ch.ChannelType() != "session" {
		ch.Reject(ssh.UnknownChannelType, "unknown channel type")
		return fmt.Errorf("denying channel type %s", ch.ChannelType())
	}

	channel, requests, err := ch.Accept()
	if err != nil {
		return err
	}

	go handleRequests(requests)
	return handleChannel(channel)
}
