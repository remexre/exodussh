package main

import (
	"io/ioutil"
	"log"
	"net"

	"golang.org/x/crypto/ssh"
)

func auth(conn ssh.ConnMetadata, password []byte) (*ssh.Permissions, error) {
	log.Printf("attempted login with user %#v password %#v", conn.User(), string(password))
	return nil, nil
}

func handleConn(conn net.Conn) error {
	config := &ssh.ServerConfig{
		PasswordCallback: auth,
	}
	privateKey, err := ioutil.ReadFile("exodussh.pem")
	if err != nil {
		return err
	}
	signer, err := ssh.ParsePrivateKey(privateKey)
	if err != nil {
		return err
	}
	config.AddHostKey(signer)
	_, chans, reqs, err := ssh.NewServerConn(conn, config)
	if err != nil {
		return err
	}
	go ssh.DiscardRequests(reqs)

	for ch := range chans {
		if err := handleNewChannel(ch); err != nil {
			log.Println(err)
		}
	}
	return nil
}
