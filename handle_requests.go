package main

import (
	"log"

	"golang.org/x/crypto/ssh"
)

func handleRequests(in <-chan *ssh.Request) {
	for req := range in {
		ok := false
		payload := []byte(nil)
		switch req.Type {
		case "exec":
			ok = true
			log.Println("#", string(req.Payload))
			payload = []byte("ExoduSSH says no!")
		case "shell":
			ok = true
			if len(req.Payload) > 0 {
				ok = false
			}
		default:
			log.Println("denying request for", req.Type)
		}
		req.Reply(ok, payload)
	}
}
