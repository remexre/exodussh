package main

import (
	"flag"
	"log"
	"net"
)

var proto = flag.String("proto", "tcp", "The protocol to listen on.")
var addr = flag.String("addr", ":22", "The address to listen on.")

func main() {
	flag.Parse()

	listener, err := net.Listen(*proto, *addr)
	if err != nil {
		log.Fatal(err)
	}

	for {
		conn, err := listener.Accept()
		if err != nil {
			log.Println(err)
		} else {
			go func(conn net.Conn) {
				if err := handleConn(conn); err != nil {
					log.Println(err)
				}
			}(conn)
		}
	}
}
