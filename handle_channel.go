package main

import (
	"log"

	"golang.org/x/crypto/ssh"
	"gopkg.in/readline.v1"
)

func handleChannel(ch ssh.Channel) error {
	config := &readline.Config{
		Prompt: "# ", // give them a feeling of power
		Stdin:  ch,
		Stdout: ch,
		Stderr: ch.Stderr(),
	}
	rl, err := readline.NewEx(config)
	if err != nil {
		return err
	}
	defer rl.Close()

	for {
		line, err := rl.Readline()
		if err != nil {
			return err
		}
		log.Println(line)
	}
}
